﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.BindModels
{
    public class EnrollmentBindModel
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public int CourseId { get; set; }

        public double Grade { get; set; }
    }
}