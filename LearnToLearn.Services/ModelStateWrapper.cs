﻿using System.Web.Http.ModelBinding;

namespace LearnToLearn.Services
{
    public class ModelStateWrapper : IValidationDictionary
    {
        private ModelStateDictionary modelState;

        public ModelStateWrapper(ModelStateDictionary modelState)
        {
            this.modelState = modelState;
        }

        public bool IsValid => this.modelState.IsValid;

        public void AddError(string key, string errorMessage)
        {
            this.modelState.AddModelError(key, errorMessage);
        }
    }
}
