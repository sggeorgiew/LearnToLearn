﻿using System;
using System.Net;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using LearnToLearn.BindModels;
using LearnToLearn.Services;
using LearnToLearn.Helpers;
using LearnToLearn.Data.Entities;

namespace LearnToLearn.Controllers
{
    [AuthorizeRoles(Roles.User, Roles.Teacher)]
    public class EnrollmentController : ApiController
    {
        private EnrollmentService _enrollmentService;

        public EnrollmentController()
        {
            _enrollmentService = new EnrollmentService(new ModelStateWrapper(this.ModelState));
        }

        [HttpPost]
        public HttpResponseMessage Enroll(EnrollmentBindModel bindModel)
        {
            if (bindModel == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            Enrollment enrollment = new Enrollment
            {
                CourseId = bindModel.CourseId,
                UserId = bindModel.UserId
            };

            if (!_enrollmentService.AddOrUpdate(enrollment))
            {
                var error = new
                {
                    Error = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage))
                };

                return Request.CreateResponse(HttpStatusCode.Forbidden, error);
            }

            return Request.CreateResponse(HttpStatusCode.OK, bindModel);
        }
    }
}
