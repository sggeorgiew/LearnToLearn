﻿using LearnToLearn.Data.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace LearnToLearn.Data
{
    public class LearnToLearnContext : DbContext
    {
        public LearnToLearnContext() : base("LearnToLearnDB")
        {
            Database.SetInitializer<LearnToLearnContext>(new CreateDatabaseIfNotExists<LearnToLearnContext>());
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
