﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace LearnToLearn.Helpers
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params Roles[] roles) : base()
        {
            var allowedRoles = roles.Select(x => Enum.GetName(typeof(Roles), x));
            Roles = String.Join(",", allowedRoles);
        }
    }
}