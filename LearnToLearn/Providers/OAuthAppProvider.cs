﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using LearnToLearn.Services;
using LearnToLearn.Data.Entities;
using LearnToLearn.Helpers;

namespace LearnToLearn.Providers
{
    public class OAuthAppProvider : OAuthAuthorizationServerProvider
    {
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return Task.Factory.StartNew(() =>
            {
                string email = context.UserName;
                string password = context.Password;
                UserService userService = new UserService();

                User user = userService.GetAll(u => u.Email == email && u.Password == password).FirstOrDefault();

                if (user != null)
                {
                    List<Claim> claims = new List<Claim>()
                    {
                        new Claim("UserId", user.Id.ToString()),
                        new Claim(ClaimTypes.Name, user.Name)
                    };

                    // Add Roles for this user
                    claims.AddRange(SetUserRoles(user));

                    ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claims, Startup.OAuthOptions.AuthenticationType);
                    context.Validated(new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties() { }));
                }
                else
                {
                    context.SetError("invalid_grant", "Error");
                }
            });
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        private List<Claim> SetUserRoles(User user)
        {
            List<Claim> claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Role, Roles.User.ToString())
            };

            if (user.IsTeacher)
            {
                claims.Add(new Claim(ClaimTypes.Role, Roles.Teacher.ToString()));
            }

            return claims;
        }
    }
}