﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LearnToLearn.BindModels;
using LearnToLearn.Services;
using LearnToLearn.Helpers;
using LearnToLearn.Data.Entities;

namespace LearnToLearn.Controllers
{
    [AuthorizeRoles(Roles.Teacher)]
    public class GradeController : ApiController
    {
        private EnrollmentService _enrollmentService;

        public GradeController()
        {
            _enrollmentService = new EnrollmentService(new ModelStateWrapper(this.ModelState));
        }

        [HttpPatch]
        public HttpResponseMessage SetGrade(int id, EnrollmentSetGradeBindModel bindModel)
        {
            Enrollment dbEnrollment = _enrollmentService.GetById(id);

            if (!_enrollmentService.Exists(dbEnrollment))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            if (!_enrollmentService.IsAuthorized(bindModel.TeacherId, dbEnrollment.CourseId))
            {
                var error = new
                {
                    Error = "You don't have rights for this course!"
                };
                
                return Request.CreateResponse(HttpStatusCode.Forbidden, error);
            }

            dbEnrollment.Grade = bindModel.Grade;
            dbEnrollment.UpdatedAt = DateTime.Now;

            if (!_enrollmentService.AddOrUpdate(dbEnrollment))
            {
                var error = new
                {
                    Error = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage))
                };

                return Request.CreateResponse(HttpStatusCode.InternalServerError, error);
            }

            return Request.CreateResponse(HttpStatusCode.OK, bindModel);
        }
    }
}
