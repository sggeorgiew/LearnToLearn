﻿namespace LearnToLearn.Data.Repositories
{
    public interface IUnitOfWork
    {
        int Save();
    }
}
