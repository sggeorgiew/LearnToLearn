﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LearnToLearn.Data.Entities
{
    public class Enrollment : BaseEntity
    {
        [Required]
        [ForeignKey("User")]
        public int UserId { get; set; }

        [Required]
        [ForeignKey("Course")]
        public int CourseId { get; set; }

        [Required]
        //[Range(2, 6)]
        public double Grade { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual User User { get; set; }
        public virtual Course Course { get; set; }

        public Enrollment()
        {
            DateTime dateTimeNow = DateTime.Now;

            CreatedAt = dateTimeNow;
            UpdatedAt = dateTimeNow;
        }
    }
}
