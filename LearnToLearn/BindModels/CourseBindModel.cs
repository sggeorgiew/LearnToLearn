﻿using System;
using LearnToLearn.Data.Entities;

namespace LearnToLearn.BindModels
{
    public class CourseBindModel
    {
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Capacity { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public CourseBindModel()
        { }

        public CourseBindModel(Course dbCourse)
        {
            Id = dbCourse.Id;
            TeacherId = dbCourse.TeacherId;
            Name = dbCourse.Name;
            Description = dbCourse.Description;
            Capacity = dbCourse.Capacity;
            CreatedAt = dbCourse.CreatedAt;
            UpdatedAt = dbCourse.UpdatedAt;
        }
    }
}