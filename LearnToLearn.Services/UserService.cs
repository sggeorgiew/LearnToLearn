﻿using System;
using System.Linq;
using System.Web.Http.ModelBinding;
using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories;

namespace LearnToLearn.Services
{
    public class UserService : BaseService<User, UserRepository, UnitOfWork>
    {
        protected override UnitOfWork _unitOfWork { get; set; }
        protected override UserRepository _repository { get; set; }

        public UserService() : base(new ModelStateWrapper(new ModelStateDictionary()))
        {
            SetUnitOfWork();
        }

        public UserService(IValidationDictionary validationDictionary) : base(validationDictionary)
        {
            SetUnitOfWork();
        }

        private void SetUnitOfWork()
        {
            _unitOfWork = new UnitOfWork();
            _repository = _unitOfWork.UserRepository;
        }

        protected override bool OnAddValidate(User userToValidate)
        {
            bool isEmailTaken = IsEmailAlreadyInUse(userToValidate.Email);

            if (isEmailTaken)
            {
                _validationDictionary.AddError("taken_email", "This email address is already taken!");
            }

            return _validationDictionary.IsValid;
        }

        protected override bool OnUpdateValidate(User userToValidate)
        {
            bool isEmailTaken = IsEmailAlreadyInUse(userToValidate.Email);
            string userName = GetById(userToValidate.Id).Name;

            if (isEmailTaken && userToValidate.Name != userName)
            {
                _validationDictionary.AddError("taken_email", "This email address is already taken!");
            }

            return _validationDictionary.IsValid;
        }

        private bool IsEmailAlreadyInUse(string email)
        {
            return GetAll(u => u.Email == email).Any();
        }
    }
}
