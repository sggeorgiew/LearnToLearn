﻿using LearnToLearn.Data.Entities;

namespace LearnToLearn.Data.Repositories
{
    public class CourseRepository : BaseRepository<Course>
    {
        public CourseRepository(LearnToLearnContext context) : base(context)
        { }
    }
}
