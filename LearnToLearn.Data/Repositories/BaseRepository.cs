﻿using System;
using System.Linq;
using System.Data.Entity;
using LearnToLearn.Data.Entities;

namespace LearnToLearn.Data.Repositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private LearnToLearnContext _context;

        public BaseRepository() : this(new LearnToLearnContext())
        { }

        public BaseRepository(LearnToLearnContext context)
        {
            _context = context;
        }

        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public IQueryable<T> GetAll(Func<T, bool> filter = null)
        {
            if (filter != null)
            {
                return _context.Set<T>().Where(filter).AsQueryable();
            }

            return _context.Set<T>().AsQueryable();
        }

        public void AddOrUpdate(T item)
        {
            if (item.Id <= 0)
            {
                Create(item);
            }
            else
            {
                Update(item);
            }
        }

        public bool DeleteById(int id)
        {
            T dbItem = _context.Set<T>().Find(id);

            if (dbItem != null)
            {
                _context.Set<T>().Remove(dbItem);
                return true;
            }

            return false;
        }

        protected virtual void Create(T item)
        {
            _context.Set<T>().Add(item);
        }

        protected virtual void Update(T item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
