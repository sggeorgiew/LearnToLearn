﻿using System.Linq;
using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories;

namespace LearnToLearn.Services
{
    public class EnrollmentService : BaseService<Enrollment, EnrollmentRepository, UnitOfWork>
    {
        protected override UnitOfWork _unitOfWork { get; set; }
        protected override EnrollmentRepository _repository { get; set; }

        public EnrollmentService(IValidationDictionary validationDictionary) : base(validationDictionary)
        {
            _unitOfWork = new UnitOfWork();
            _repository = _unitOfWork.EnrollmentRepository;
        }

        public bool IsAuthorized(int teacherId, int courseId)
        {
            Course course = _unitOfWork.CourseRepository.GetById(courseId);

            if (course.TeacherId != teacherId)
            {
                return false;
            }

            return true;
        }

        protected override bool OnAddValidate(Enrollment enrollmentToValidate)
        {
            int courseId = enrollmentToValidate.CourseId;
            int userId = enrollmentToValidate.UserId;

            if (IsAlreadyEnrolled(courseId, userId))
            {
                _validationDictionary.AddError("already_enrolled", "You are already enrolled to this course!");
            }
            else if (!HasFreePlace(courseId))
            {
                _validationDictionary.AddError("no_place", "We are sorry, but all places are already taken!");
            }

            return _validationDictionary.IsValid;
        }

        protected override bool OnUpdateValidate(Enrollment enrollmentToValidate)
        {
            double grade = enrollmentToValidate.Grade;

            if (grade < 2 || grade > 6)
            {
                _validationDictionary.AddError("invalid_grade", "The grade must be between 2 and 6!");
            }

            return _validationDictionary.IsValid;
        }

        private bool IsAlreadyEnrolled(int courseId, int userId)
        {
            Enrollment enrollment = GetAll(e => e.CourseId == courseId && e.UserId == userId).FirstOrDefault();

            if (enrollment == null)
            {
                return false;
            }

            return true;
        }

        private bool HasFreePlace(int courseId)
        {
            var enrollments = _repository.GetAll(e => e.CourseId == courseId);
            Course course = _unitOfWork.CourseRepository.GetById(courseId);

            if (enrollments.Count() >= course.Capacity)
            {
                return false;
            }

            return true;
        }
    }
}
