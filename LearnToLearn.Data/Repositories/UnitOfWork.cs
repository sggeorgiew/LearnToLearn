﻿namespace LearnToLearn.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private LearnToLearnContext _context = new LearnToLearnContext();

        private UserRepository _userRepository;
        private CourseRepository _courseRepository;
        private EnrollmentRepository _enrollmentRepository;

        public UserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_context);
                }

                return _userRepository;
            }
        }

        public CourseRepository CourseRepository
        {
            get
            {
                if (_courseRepository == null)
                {
                    _courseRepository = new CourseRepository(_context);
                }

                return _courseRepository;
            }
        }

        public EnrollmentRepository EnrollmentRepository
        {
            get
            {
                if (_enrollmentRepository == null)
                {
                    _enrollmentRepository = new EnrollmentRepository(_context);
                }

                return _enrollmentRepository;
            }
        }

        public int Save()
        {
            return _context.SaveChanges();
        }
    }
}
