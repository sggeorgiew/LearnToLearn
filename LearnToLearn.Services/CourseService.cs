﻿using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories;

namespace LearnToLearn.Services
{
    public class CourseService : BaseService<Course, CourseRepository, UnitOfWork>
    {
        protected override UnitOfWork _unitOfWork { get; set; }
        protected override CourseRepository _repository { get; set; }

        public int? OldCourseCapacity { get; set; }

        public CourseService(IValidationDictionary validationDictionary) : base(validationDictionary)
        {
            _unitOfWork = new UnitOfWork();
            _repository = _unitOfWork.CourseRepository;
        }

        protected override bool OnAddValidate(Course courseToValidate)
        {
            if (courseToValidate.Capacity <= 0)
            {
                _validationDictionary.AddError("invalid_capacity", "The course capacity must be greater than 0!");
            }

            User teacher = _unitOfWork.UserRepository.GetById(courseToValidate.TeacherId);

            if (teacher != null)
            {
                if (!teacher.IsTeacher)
                {
                    _validationDictionary.AddError("insufficient_rights", "You are not permitted to create a course!");
                }
            }

            return _validationDictionary.IsValid;
        }

        protected override bool OnUpdateValidate(Course courseToValidate)
        {
            int newCourseCapacity = courseToValidate.Capacity;

            if (newCourseCapacity < OldCourseCapacity)
            {
                if (courseToValidate.Enrollments.Count > newCourseCapacity)
                {
                    _validationDictionary.AddError("invalid_capacity", "There are too many users in the course. You can't reduce the capacity!");
                }
            }

            return _validationDictionary.IsValid;
        }
    }
}
