namespace LearnToLearn.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCourseVisibility : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Courses", "IsVisible", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Courses", "IsVisible");
        }
    }
}
