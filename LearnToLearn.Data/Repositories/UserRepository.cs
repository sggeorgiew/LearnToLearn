﻿using LearnToLearn.Data.Entities;

namespace LearnToLearn.Data.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(LearnToLearnContext context) : base(context)
        { }
    }
}
