﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.BindModels
{
    public class CourseCreateBindModel
    {
        [Required]
        public int TeacherId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public int Capacity { get; set; }

        public bool IsVisible { get; set; }

        public CourseCreateBindModel()
        {
            IsVisible = true;
        }
    }
}